$(document).ready(function() {
    "use strict";

    var window_width = $(window).width(),
        window_height = window.innerHeight,
        header_height = $(".default-header").height(),
        header_height_static = $(".site-header.static").outerHeight(),
        fitscreen = window_height - header_height;

    $(".fullscreen").css("height", window_height)
    $(".fitscreen").css("height", fitscreen);

    //------- Niceselect  js --------//  

    if (document.getElementById("default-select")) {
        $('select').niceSelect();
    };
    if (document.getElementById("default-select2")) {
        $('select').niceSelect();
    };
    if (document.getElementById("service-select")) {
        $('select').niceSelect();
    };    

    //------- Lightbox  js --------//  

    $('.img-gal').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });

    $('.play-btn').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });

    //------- Datepicker  js --------//  

      $( function() {
        $( "#datepicker" ).datepicker();
        $( "#datepicker2" ).datepicker();
      } );


    //------- Superfist nav menu  js --------//  

    $('.nav-menu').superfish({
        animation: {
            opacity: 'show'
        },
        speed: 400
    });

    /* ---------------------------------------------
     accordion
     --------------------------------------------- */

    var allPanels = $(".accordion > dd").hide();
    allPanels.first().slideDown("easeOutExpo");
    $(".accordion").each(function() {
        $(this).find("dt > a").first().addClass("active").parent().next().css({
            display: "block"
        });
    });


     $(document).on('click', '.accordion > dt > a', function(e) {

        var current = $(this).parent().next("dd");
        $(this).parents(".accordion").find("dt > a").removeClass("active");
        $(this).addClass("active");
        $(this).parents(".accordion").find("dd").slideUp("easeInExpo");
        $(this).parent().next().slideDown("easeOutExpo");

        return false;

    });

    //------- Tabs Js --------//  
    if (document.getElementById("horizontalTab")) {

    $('#horizontalTab').jqTabs({
        direction: 'horizontal',
        duration: 200
    });
    
    };  


    //------- Owl Carusel  js --------//  


    $('.active-popular-carusel').owlCarousel({
        items:4,
        margin: 30,
        loop:true,
        dots: true,
        autoplayHoverPause: true,
        smartSpeed:650,         
        autoplay:true,      
            responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1,
            },
            768: {
                items: 2,
            },
            992: {
                items:4
            }
        }
    });

    $('.active-upcoming-event-carusel').owlCarousel({
        items:2,
        margin: 30,
        loop:true,
        dots: true,
        autoplayHoverPause: true,
        smartSpeed:650,         
        autoplay:true,      
            responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1,
            },
            992: {
                items: 2,
            }
        }
    });


    $('.active-review-carusel').owlCarousel({
        items:2,
        margin: 30,
        loop:true,
        dots: true,
        autoplayHoverPause: true,
        smartSpeed:650,         
        autoplay:true,      
            responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1,
            },
            768: {
                items: 2,
            }
        }
    });

    //------- Mobile Nav  js --------//  

    if ($('#nav-menu-container').length) {
        var $mobile_nav = $('#nav-menu-container').clone().prop({
            id: 'mobile-nav'
        });
        $mobile_nav.find('> ul').attr({
            'class': '',
            'id': ''
        });
        $('body').append($mobile_nav);
        $('body').prepend('<button type="button" id="mobile-nav-toggle"><i class="lnr lnr-menu"></i></button>');
        $('body').append('<div id="mobile-body-overly"></div>');
        $('#mobile-nav').find('.menu-has-children').prepend('<i class="lnr lnr-chevron-down"></i>');

        $(document).on('click', '.menu-has-children i', function(e) {
            $(this).next().toggleClass('menu-item-active');
            $(this).nextAll('ul').eq(0).slideToggle();
            $(this).toggleClass("lnr-chevron-up lnr-chevron-down");
        });

        $(document).on('click', '#mobile-nav-toggle', function(e) {
            $('body').toggleClass('mobile-nav-active');
            $('#mobile-nav-toggle i').toggleClass('lnr-cross lnr-menu');
            $('#mobile-body-overly').toggle();
        });

            $(document).on('click', function(e) {
            var container = $("#mobile-nav, #mobile-nav-toggle");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                if ($('body').hasClass('mobile-nav-active')) {
                    $('body').removeClass('mobile-nav-active');
                    $('#mobile-nav-toggle i').toggleClass('lnr-cross lnr-menu');
                    $('#mobile-body-overly').fadeOut();
                }
            }
        });
    } else if ($("#mobile-nav, #mobile-nav-toggle").length) {
        $("#mobile-nav, #mobile-nav-toggle").hide();
    }

    //------- Smooth Scroll  js --------//  

    $('.nav-menu a, #mobile-nav a, .scrollto').on('click', function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            if (target.length) {
                var top_space = 0;

                if ($('#header').length) {
                    top_space = $('#header').outerHeight();

                    if (!$('#header').hasClass('header-fixed')) {
                        top_space = top_space;
                    }
                }

                $('html, body').animate({
                    scrollTop: target.offset().top - top_space
                }, 1500, 'easeInOutExpo');

                if ($(this).parents('.nav-menu').length) {
                    $('.nav-menu .menu-active').removeClass('menu-active');
                    $(this).closest('li').addClass('menu-active');
                }

                if ($('body').hasClass('mobile-nav-active')) {
                    $('body').removeClass('mobile-nav-active');
                    $('#mobile-nav-toggle i').toggleClass('lnr-times lnr-bars');
                    $('#mobile-body-overly').fadeOut();
                }
                return false;
            }
        }
    });

    $(document).ready(function() {

        $('html, body').hide();

        if (window.location.hash) {

            setTimeout(function() {

                $('html, body').scrollTop(0).show();

                $('html, body').animate({

                    scrollTop: $(window.location.hash).offset().top - 108

                }, 1000)

            }, 0);

        } else {

            $('html, body').show();

        }

    });



    //------- Header Scroll Class  js --------//  

    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('#header').addClass('header-scrolled');
        } else {
            $('#header').removeClass('header-scrolled');
        }
    });

    //------- Google Map  js --------//  

    if (document.getElementById("map-bak")) {
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            var mapOptions = {
                zoom: 11,
                center: new google.maps.LatLng(40.6700, -73.9400), // New York
                styles: [{
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#e9e9e9"
                    }, {
                        "lightness": 17
                    }]
                }, {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f5f5f5"
                    }, {
                        "lightness": 20
                    }]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 17
                    }]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 29
                    }, {
                        "weight": 0.2
                    }]
                }, {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 18
                    }]
                }, {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 16
                    }]
                }, {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f5f5f5"
                    }, {
                        "lightness": 21
                    }]
                }, {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#dedede"
                    }, {
                        "lightness": 21
                    }]
                }, {
                    "elementType": "labels.text.stroke",
                    "stylers": [{
                        "visibility": "on"
                    }, {
                        "color": "#ffffff"
                    }, {
                        "lightness": 16
                    }]
                }, {
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "saturation": 36
                    }, {
                        "color": "#333333"
                    }, {
                        "lightness": 40
                    }]
                }, {
                    "elementType": "labels.icon",
                    "stylers": [{
                        "visibility": "off"
                    }]
                }, {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f2f2f2"
                    }, {
                        "lightness": 19
                    }]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#fefefe"
                    }, {
                        "lightness": 20
                    }]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "color": "#fefefe"
                    }, {
                        "lightness": 17
                    }, {
                        "weight": 1.2
                    }]
                }]
            };
            var mapElement = document.getElementById('map');
            var map = new google.maps.Map(mapElement, mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(40.6700, -73.9400),
                map: map,
                title: 'Snazzy!'
            });
        }
    }

    //------- Mailchimp js --------//  

    $(document).ready(function() {
        $('#mc_embed_signup').find('form').ajaxChimp();
    });

    //------- Mailchimp js --------//  

    $(document).ready(function() {
        $('#slydepay-form').find('form').ajaxChimp();
    });

    // return this.http.post(H+"api/merchant/paypoint/invoice/checkstatus",e,n)
    // .map(function(e){return e.json()}),e.prototype.createInvoice=function(e){var t=new Y.a({"Content-Type":"application/json",Accept:"application/json"}),n=new Y.c({headers:t});return this.http.post(e,"",n).map(function(e){return e.json()})},e.prototype.sendInvoice=function(e){var t=new Y.a({"Content-Type":"application/json",Accept:"application/json"}),n=new Y.c({headers:t});

    const ONLINE_OPTIONS = ['SLYDEPAY', 'VISA'];
    const MOBILE_MONEY_OPTIONS = ['MTN_MONEY', 'VODAFONE_CASH', 'TIGO_CASH', 'AIRTEL_MONEY'];

    // $("#pay-option-select").addEventListener('change', showRequiredField);
    $("#pay-option-select").on('change', showRequiredField);

    function showRequiredField(e){
        e.preventDefault();
        
        // var payOptionEl = document.getElementById('pay-option-select');
        console.log("target.value: ", e.target.value);
        console.log("ONLINE_OPTIONS.includes(e.target.value): ", ONLINE_OPTIONS.includes(e.target.value));
        console.log("MOBILE_MONEY_OPTIONS.includes(e.target.value): ", MOBILE_MONEY_OPTIONS.includes(e.target.value));
        console.log("$(#email).is(':hidden'): ", $("#email").is(':hidden'));
        console.log("$(#phone).is(':hidden'): ", $("#phone").is(':hidden'));

        if(MOBILE_MONEY_OPTIONS.includes(e.target.value) && $("#phone").is(':hidden')){
            $("#phone").fadeIn().removeClass("d-none");
            $("#email").fadeOut().addClass("d-none");
            e.target.setCustomValidity("");
            // $("#phone").show();
            // $( "#phone" ).toggle();
            console.log("Show phone input");
            return false;
        } else if(ONLINE_OPTIONS.includes(e.target.value) && $("#email").is(':hidden')){
            $("#email").fadeIn().removeClass("d-none");
            $("#phone").fadeOut().addClass("d-none");
            e.target.setCustomValidity("");
            console.log("Show email input");
            return false;
        } else if(e.target.value === '') {
            // var s = document.getElementById('phone')
            $("#phone").fadeOut().addClass("d-none");//and clear content anytime u hide
            $("#email").fadeOut().addClass("d-none");
            e.target.setCustomValidity("Choose a payment method.");
            console.log("keep mute");
            return false;
        }else{
            console.log("In a state of SHOCK!");
        }
    }

    $("#phone").on('focus', removeCustomValidity);
    $("#email").on('focus', removeCustomValidity);
    function removeCustomValidity(e){
        e.preventDefault();
        e.target.setCustomValidity("");
    }

    //TODO: Disable button to prevent further clicks
    $("#pay-form").on('submit', function(e){
        e.preventDefault();
        axios('http://localhost:8080/quickPay')
        .then(function (response) {
            console.log("response", response)
            if(response.data.status){
                //TODO: Clear form
                // userPage(response.data);
                //Ensure user cannot access without logging in
                window.location.href = 'user.html';
                $("#user-available-seats").val(response.data.remainingSeats);
                $("#local-fee").val(response.data.locFee);
                $("#foreign-fee").val(response.data.forFee);
            }else{
                console.log("response", "figure it out", response)
            }
        }).catch(function (error) {
            console.log("asf-error", error)
        })
    });

    

    // function userPage(userPageInfo){
    //     console.log("going to user page")
    //     axios('http://localhost:8080/user')
    //     .then(function (response) {
    //         console.log("userPageInfo", userPageInfo)
    //         console.log("response", response)
    //         if(response.data.status){
    //             console.log("response", "good")
    //         }else{
    //             console.log("response", "figure it out")
    //         }
    //     }).catch(function (error) {
    //         console.log("asf-error", error)
    //     })
    // }
    // $("#pay-form").on('submit', performPostRequest);
    // $("#pay-form").on('submit', function(e){
    //     e.preventDefault();
    //     axios('http://localhost:8080/get')
    //     .then(function (response) {
    //         console.log("response", response)
    //     }).catch(function (error) {
    //         console.log("asf-error", error)
    //     })
    // });

    function performPostRequest(e) {
        // var resultElement = document.getElementById('postResult');
        // var todoTitle = document.getElementById('todoTitle').value;
        e.preventDefault();

        var name = document.getElementById('name').value;
        var phoneEl = document.getElementById('phone');
        var emailEl = document.getElementById('email');
        var payOptionEl = document.getElementById('pay-option-select');
        var accSession = document.getElementById('acc-session-select').value;
        // resultElement.innerHTML = '';
        console.log("name:", name)
        console.log("phone:", phoneEl.value)
        console.log("email:", emailEl.value)
        console.log("payOption:", payOptionEl.value)
        console.log("accSession:", accSession)
        // visa, mtn, airtel, slydepay
        // ZENITH_VISA, SLYDEPAY
        

        // TODO: Notify user if mobile money option is used

        if(MOBILE_MONEY_OPTIONS.includes(payOptionEl.value) && '' === phoneEl.value){
            phoneEl.setCustomValidity("Please enter your phone number");
            payOptionEl.setCustomValidity("");
            emailEl.setCustomValidity("");
            console.log("require phone");
            return false;
        } else if(ONLINE_OPTIONS.includes(payOptionEl.value) && '' === emailEl.value){
            emailEl.setCustomValidity("Please enter your email address");
            payOptionEl.setCustomValidity("");
            phoneEl.setCustomValidity("");
            console.log("require email");
            return false;
        }
        // else {
        //     console.log("Sustained SHOCK! => PPR");
        // }

        // console.log("won't get here if it's failed momo attempt ");
        
        // axios.post('http://jsonplaceholder.typicode.com/todos', {
            // const options = {
            //     method: 'POST',
            //     headers: { 'content-type': 'application/x-www-form-urlencoded' },
            //     data: qs.stringify(data),
            //     url,
            //   };
            //   axios(options);

        const options = { 
            // headers: {'vsofwebtoken': '9338b619-8359-4322-bfd5-7cd6ae7f9000'},
            headers: {'Content-Type': 'application/json'}
            // transformRequest: [function (data, headers) {
            //     // Do whatever you want to transform the data
            
            //     console.log("hearrd--eerrs", headers);
            //     return data;
            //   }],
            }
        axios.post('http://localhost:8080/buy', {
          name: name,
          email: emailEl.value,
          mobile: phoneEl.value,
          session: accSession,
          channel: payOptionEl.value,
        }, options)
        .then(function (response) {
            console.log("success:" + response.data);
        //   resultElement.innerHTML = generateSuccessHTMLOutput(response);
        })
        .catch(function (error) {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log("wan");
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
                console.log('Error', error.message);
              } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log("two");
                console.log(error.request);
                console.log('Error', error.message);
              } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', "three");
                console.log('Error', error.message);
              }
              console.log("finale");
              console.log(error.config);
        });
      }

      $("#down-forms").on('click', function(e){
          e.preventDefault();
          console.log("#down-forms");
          axios.post('http://localhost:9010/post')
        //   axios.post('https://vsofapp.herokuapp.com/download')
          .then(function(response){
            console.log("response: ", response);
          }).catch(function(error){
            console.log("error", error);
          })
      });

    //   $("#down-forms-1").on('click', function(e){
    //       e.preventDefault();
    //       console.log("#down-forms");
    //       axios.post('http://localhost:9010/forms/application')
    //       .then(function(response){
    //         console.log("response: ", response);
    //       }).catch(function(error){
    //         console.log("error", error);
    //       })
    //   });
});